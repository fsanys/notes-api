import React, { useState, useEffect } from "react"
import "./App.css"
import APIHelper from "./APIHelper.js"

function App() {
  const [notes, setNotes] = useState([])
  const [note, setNote] = useState("")

  useEffect(() => {
    const fetchNoteAndSetNotes = async () => {
      const notes = await APIHelper.getAllNotes()
      setNotes(notes)
    }
    fetchNoteAndSetNotes()
  }, [])

  const createNote = async e => {
    e.preventDefault()
    if (!note) {
      alert("please enter something")
      return
    }
    if (notes.some(({ matiere }) => matiere === note)) {
      alert(`Task: ${note} already exists`)
      return
    }
    const newNote = await APIHelper.createNote(note)
    setNotes([...notes, newNote])
  }

  const deleteNote = async (e, id) => {
    try {
      e.stopPropagation()
      await APIHelper.deleteNote(id)
      setNotes(notes.filter(({ _id: i }) => id !== i))
    } catch (err) {}
  }

  const updateNote = async (e, id) => {
    e.stopPropagation()
    const payload = {
      moyenne: !notes.find(note => note._id === id).completed,
    }
    const updatedNote = await APIHelper.updateNote(id, payload)
    setNotes(notes.map(note => (note._id === id ? updatedNote : note)))
  }

  return (
    <div className="App">
      <div>
        <input
          id="note-input"
          type="text"
          value={note}
          onChange={({ target }) => setNote(target.value)}
        />
        <button type="button" onClick={createNote}>
          Add
        </button>
      </div>

      <ul>
        {notes.map(({ _id, matiere, moyenne }, i) => (
          <li
            key={i}
            onClick={e => updateNote(e, _id)}
            className={moyenne ? "moyenne" : ""}
          >
            {matiere} <span onClick={e => deleteNote(e, _id)}>X</span>
          </li>
        ))}
      </ul>
    </div>
  )
}

export default App
