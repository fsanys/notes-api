const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const PORT = process.env.PORT || 3000;
const db = require("./models/");
const cors = require("cors");

app.use(cors());
app.use(bodyParser.json());

function success(res, payload) {
  return res.status(200).json(payload);
}

app.get("/notes", async (req, res, next) => {
  try {
    const notes = await db.Note.find({});
    return success(res, notes);
  } catch (err) {
    next({ status: 400, message: "failed to get notes" });
  }
});

app.post("/notes", async (req, res, next) => {
  try {
    const notes = await db.Note.create(req.body);
    return success(res, notes);
  } catch (err) {
    next({ status: 400, message: "failed to create notes" });
  }
});

app.put("/notes/:id", async (req, res, next) => {
  try {
    const note = await db.Note.findByIdAndUpdate(req.params.id, req.body, {
      new: true
    });
    return success(res, note);
  } catch (err) {
    next({ status: 400, message: "failed to update note" });
  }
});
app.delete("/notes/:id", async (req, res, next) => {
  try {
    await db.Note.findByIdAndRemove(req.params.id);
    return success(res, "note deleted!");
  } catch (err) {
    next({ status: 400, message: "failed to delete note" });
  }
});

app.use((err, req, res, next) => {
  return res.status(err.status || 400).json({
    status: err.status || 400,
    message: err.message || "there was an error processing request"
  });
});

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});

