const mongoose = require("mongoose") // requiring the mongoose package

const noteSchema = new mongoose.Schema({
  // creating a schema for todo
  matiere: {
    // field1: task
    type: String, // task is a string
    unique: true, // it has to be unique
    required: true, // it is required
  },
  moyenne: {
    // field2: completed
    type: Number, // it is a boolean
    default: false, // the default is false
  },
})

const noteModel = mongoose.model("note", noteSchema) // creating the model from the schema

module.exports = noteModel // exporting the model
